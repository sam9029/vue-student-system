# 本项目的接口文档

### 后端项目默认端口号`3000`

### 使用nodejs的express搭建的服务器

### 数据库是mongoDB

### 本项目关联数据表 都是关联的数据表id

# 通用数据

- 请在前端储存通用数据
- 分页
- 搜索

```js
pageData = {
	//当前页
	currentPage:1,
	//每页数据条数
	pageSize:5,
	//先存着，页面初始渲染会赋值
	totalPage:0,
	totalStudent:0,
	
	//先存着，type默认为name,
	searchType:'name',
	searchValue:'',
}
```

# 所有请求返回数据

```js
{
	message:'反馈信息',
	status: 0/1
	//0 请求不合法 或 失败
    //1  请求成功
}
```



# students

### 1. 获取所有学生
- 关联班级-获取时同时返回学生所在班级的信息
~~~JS
url: '/students/getStudents',
type: 'get',
~~~

```js
{
	//当前页（使用分页必填）
	currentPage:1,
	//每页数据条数（使用分页必填）
	pageSize:5,
}
```

### 2. 新增学生
~~~JS
	url:'/students/addStudent',
	type:'post',
~~~

```js
{
    name, //必填
    age, //必填
    gender, //必填
    classId, //必填
    imagesName
},
```

### 3. 根据 id 获取 某个 学生

~~~JS
	url:'/students/_id',
	type:'get',
~~~

```js
{
	_id, //必填
}
```

### 4. 修改学生

```js
url:'/students/upgradeStuInfo',
type:'post',
```

```js
{
    _id, //必填
    name, //必填
    age, //必填
    gender, //必填
    classId, //必填
    imagesName
},
```

### 5. 删除学生

```js
url:'/students/deleteStudent',
type:'get',
```

```js
{
	_id, //必填
}
```

### 4. 搜索学生

- 特殊-和获取学生走一条路径
- 后端根据 searchValue 有无 识别获取 于 搜索
- ⭐注意获取前修改页码为 1

```js
url:'students/getStudents',
type:'get',
```

```js
data:{
	//当前页（使用分页必填）
	currentPage:1,
	//每页数据条数（使用分页必填）
	pageSize:5,	
	searchType, //必填
	searchValue, //必填
},
```



# teachers

### 获取所有老师

```js
url:'/teachers/getTeachers',
type:'get'
```

```js
//无参数
```

### 新增老师

```js
url:'/teachers/addTeacher',
type:'post',
```

```js
{
	name, //必填
},
```



# classes
### 获取所有班级

```js
url:'/classes/getClasses',
type:'get'
```

```js
//无参数
```

### 新增班级

- 参数里的 teachers 可以是数组，字符串

```js
url:'/classes/addClass',
type:'post',
```

```js
{
	name, //必填
    teachers, //必填
},
```

# users

### 登录请求

```js
url:'/users/login',
type:'get',
```

```js
{
	username,
	password
}
```

### 用户名存在验证

```js
url:'/users/isExist',
type:'get',
```

```js
{
	username
},
```

### 注册请求

```js
url:'users/register',
type:'post',
```

```js
{
    reusername,
    repassword,
    copassword,
}
```

### 登录验证-token  UN

- 有许多注意事项，待写完
- 后端设置不需要进行身份认证的接口
`path: ['/users/login', '/users/register','/users/isExist']`

```js
url:'users/isLogin',
type:'get',
headers:{
    //要加  'Bearer ' 在前面
    //Bearer后面要用空格，
    Authorization: 'Bearer ' + localStorage.getItem('token')
},
```

```
//无参
```

```js
res.send({
    message:'用户信息获取成功',
    status: 1,
    data:{
      username
    }
  })
```




# images-UN

- 有 工具 处理 （待写完）
- node 插件 multer
- 使用 JS 原生formData 构造函数 传输图片
- 图片请求的ajax 特殊处理

### 图片上传

```js
url:'/images/uploadImg',
//图片上传一定要用post
type:'post',
//使用JS 原生表单对象 formData 传输图片数据
data:formData,

//以下三个默认加上
cache:false, //不读取缓存结果
contentType:false, //数据编码不适用jquery格式
processData:false, //发送图片不转换其他格式
```

```js
//data:formData,
```



---

# 数据库

数据库名`studentSystem`

```
- users
- students
- teachers
- classes
```

## users

```
username: String, 
password:String,
```

## students

```js
name: String,
age: String,
gender: String,
classId: String, //作关联
imagesName:String
```

## teachers

```js
name: String,
```

## classes

```js
name: String,
teachers:Array/String //作关联
```

