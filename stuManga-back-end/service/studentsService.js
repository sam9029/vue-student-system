const {
	getStudents,
	searchStudents,
	addStudent,
	upgradeGetStudent,
	upgradeStuInfo,
	deleteStudent } = require('../dao/studentsDao.js');

//获取或搜索所有学生
module.exports.getStudents = async params => {
	//params={
	// currentPage,
	// pageSize,
	// totalPage, 
	// totalStudent,
	// searchValue,
	// searchType
	// }

	//判断params.searchValue是否存在，来区别 获取 与 搜索	
	if (params.searchValue) {
		console.log('222-------走了搜索');
		console.log(params);
		const data = await searchStudents(params);
		// console.log("搜索结果",data);
		return {
			message: '搜索学生数据-请求成功',
			status: 1,
			data
		}
	} else {
		console.log('222-------走了获取');
		console.log(params);
		const data = await getStudents(params);
		return {
			message: '获取所有学生数据-请求成功',
			status: 1,
			data
		}
	}
}

//搜索学生--目前已弃用
// module.exports.searchStudents = async params =>{
// 	if(params.searchValue){
// 		const result = await searchStudents(params);
// 		return {
// 			message:'搜索所有学生数据-请求成功',
// 	        status:1,
// 	        result
// 		}
// 	}
// 	else{
// 		return {
// 			message:'搜索值为空',
// 	        status:0,
// 		}
// 	}
// }

//新增学生
module.exports.addStudent = async params => {
	//添加数据不为空
	if (params.name && params.age && params.gender && params.classId) {
		//年龄正负判断
		if (+ params.age > 0 && + params.age <= 100) {
			const data = await addStudent(params);
			return {
				message: '新增学生数据-成功',
				status: 1,
				data
			};
		} else {
			return {
				message: '注意学生年龄不能为负数,或者超过100',
				status: 0,
			};
		}
	} else {
		return {
			message: '输入信息为空！',
			status: 0
		}
	}
}

//修改学生--通过_id获取学生信息
module.exports.upgradeGetStudent = async params => {
	if (params._id) {
		const data = await upgradeGetStudent(params);
		if (data) {
			return {
				message: '通过_id获取学生信息-成功',
				status: 1,
				data
			};
		} else {
			return {
				message: '通过此_id获取为空，无该_id学生信息',
				status: 0,
			};
		}
	} else {
		return {
			message: '请求_id为空,获取学生信息-失败',
			status: 0,
		};
	}
}

//修改学生
module.exports.upgradeStuInfo = async params => {
	if (params._id && params.name && params.age && params.gender) {
		const data = await upgradeStuInfo(params);
		return {
			message: '修改学生信息-成功',
			status: 1,
			data
		};
	} else {
		return {
			message: '信息为空或错误学生信息，请在列表选择要修改的学生',
			status: 0,
		};
	}
}

//删除学生
module.exports.deleteStudent = async params => {
	const data = await deleteStudent(params);
	return {
		message: '删除成功！',
		status: 1,
		data
	};
}