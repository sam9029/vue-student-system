> p370-p

# 版本 1.0

# 功能说明
 - 更新
 - 关键词
 - 技术栈
 - 功能详细说明
 - 错误总结
 - 遗留待优化

## 更新
	- 获取班级信息时，同时返回班级人数信息
	- UN-新增学生 时 年龄 正负判断

## 关键词
```
 - 前端
 	- 渲染 ~~~js Aarry.map(item=>{return `code`}).join('') ~~~
 	- 分页时，最后一页只剩一条数据，优化（判断返回当前数据条数小于1，本地储蓄的currentPage-1，即可回退一页）
 - 后台
 	- 数据表关联populate
 	- 获取学生与搜索走的一个url，根据是否传值来区别获取与搜索
 		- 数据库操作Dao 两个方法 getStudents/searchStudents
 	- 数据表搜索（模糊查询） 正则匹配
 	~~~js
 	模型.find({
 		//正则实现模糊查询，option不区分大小写
        [type]: { $regex: value, $options: '$i' }
    })
    ~~~
    - !!!搜索的分页 大成功--ok
    	- 解决方案（合并表现层和服务层api，检测搜索框的变化并本地存值）
        	- 搜索框值检测，并本地保存，ajax时会被传送
        	- 后台搜索框值判断，有值走搜索，无值走正常 获取
        	- 分页时，搜索框若有值在，就可以使用了
        	- 表现层和服务层共用api，持久层分了`
        	- 退出搜索状态，利用本地搜索框值检测
	- 图片上传
		- 本是一个复制的过程
		- node 插件 multer
		- 封装的函数工具
		- JS 原生formData 构造函数
		- input type='file'
		- 图片请求的ajax 特殊处理``
	- 上传图片后预览
	- 加密处理
		-注册 在服务层service层加密处理
		-登录 在服务层service层加密后对比验证
	- token验证(！！有待加深)
		- 在 ajax headers中携带
			- Authorization要加 "bearer " (记得空格)
			- 统一处理所有Ajax
```


## 技术栈
 - node
 	- jquery
 	- express
 	- express-generator
 	- monogoose
 - mongoDB

## 数据库拓展：
### 1. 关联数据表
~~~js
//在 model 中 关联：

//本次实例--学生表的关联班级数据
const classesSchema = new Schema({
	name:String,
	age:String,
	gender:String,
	//关联班级数据
	classId:{
		type:Schema.Types.ObjectId,
		ref:'classesModel'
	}
})
~~~

### 2. mongoose 数据表 关联其他表 查询数据
~~~js
//注意先要在model关联, 即 1. 关联数据表 里面的操作

//语法
模型.find().populate(path, [select],[model],[match],[options])

1.path  指定要查询的表

2.select(可选)  指定要查询的字段

3.model(可选)  类型：Model，可选，指定关联字段的 model，如果没有指定就会使用Schema的ref。

4.match(可选)  类型：Object，可选，指定附加的查询条件。

5.options(可选)  类型：Object，可选，指定附加的其他查询选项，如排序以及条数限制等等。
~~~

#### 多级关联查询
~~~JS
//实例--学生表的查询同时查询关联班级数据
//studentsModel={name,age,gender,classId}
//classesModel={name,teachers}
//teachersModel={name}

//一级
//只查询 学生表 关联 班级表
const students = await studentsModel.find().populate('classId');

//二级
//查询 学生表 关联 班级表 关联 教师表
const students = await studentsModel.find().populate({
    path:'classId',
    populate:{
        path:'teachers'
    }
});
~~~

### 3. mongoDB数据表-存储-数组数据
~~~js
//在Schema 和 model 中 声明：

const xxxSchema = new Schema({
	arrName:Array;
})

//本次实例--班级表的关联老师数据（数组）
const classesSchema = new Schema({
	name:String,
	teacherId:[{
		type:Schema.Type.ObjectId,
		ref:'teachersModel'
	}]
})
~~~


---
## 功能详细说明

### 数据表
```
- 学生表-关联班级表
	- model 里面 改 `属性:{type:Schema.Types.ObjectId,ref:'关联model'}`
- 班级表-关联教师表（数组）
	- model 里面 改 `数组属性:[ {type:Schema.Types.ObjectId,ref:'关联model'} ]`
- 教师表
```

### 学生列表渲染
```
- 获取所有学生信息（学生表关联班级表（班级表关联教师表））-OK
	- 关联查询的时候,其他关联查询也类似
	~~~JS
	model
	.find()
    .populate({
        path:'classId',
        populate:{
            path:'teachers'
        }
    })
	~~~
- 加载页面后渲染--ok
 - 使用单独的渲染函数（搜索共用）
- 分页 -OK
	- 本地存储 `currentPage,totalPage,pageSize`信息
	- 控制每页数据条数mongoose插件
	~~~JS
	model.操作()
	.limit(pageSize - 0)  // 设置请求数据的条数  "- 0"是转换字符串为数值
	.skip((currentPage - 1) * pageSize)  // 设置跳过数据的条数;
	~~~
- 分页时 
	-（最后一页只剩一条数据化）-ok
```
### 新增学生
```
- 新增-ok
- 新增完成清空修改栏-ok
- 新增完成更新学生列表-ok
- 关联班级（班级关联教师-ok）-ok
```
### 修改学生
```
- 修改前取学生信息并渲染至修改区-ok
- 修改完成清空修改栏-ok
- 修改完成更新学生列表-ok
```
### ⭐学生信息搜索
```
- 关联搜索-ok
- 搜索后使用分页功能-ok
	- 初次搜索需要将现页码改为 1
	- 本地存储 `searchValue,searchType`信息
	- 搜索时携带本地信息，后端根据searchValue有无走 获取 和 搜索
```
### 新增老师
```
- 新增-ok
```
### 老师选项渲染至`班级区`
```
- 加载页面后-获取所有老师信息并渲染-ok
```
### 新增班级
```
- 关联教师（数组）-ok
- ajax 请求发送数据 有 数组形式 时`data:{}//后面加,traditional:true`
```
### 班级选项渲染至学生`新增/修改区`
```
- 加载页面后-获取所有班级信息并渲染新增/修改区-ok
```
### 上传图片功能
```
- 上传并使界面预览-ok
	- 选择好图片后就已经上传了，新增时只上传后的文件名即可 -ok
- 利用临时文件夹暂存图片 -ok
- 新增后将图片移入img文件夹 -ok
```
### 加密处理
```
- 注册 在服务层service层加密处理 -ok
- 登录 在服务层service层加密后对比验证 -ok
```

### token验证
```
- token 加密 
- 
- 统一ajax token处理
```
---

## 错误和总结

### 1. 持久层Dao文件引入model 一定要解构来引入
~~~js
//正确
const {studentsModel} = require('./models/studentsModel');

//错误
//const studentsModel = require('./models/studentsModel');
~~~

### 2. 路由router 文件 要暴露
~~~js
module.exports = router;
~~~

### 3. 数据不为空处理
~~~js	
//添加数据不为空
if(params.name && params.age && params.gender){//code---}

//下面就不行,不行有待探究
//if(params.name==true && params.age==true && params.gender==true){//code---}
//if(params.name===true && params.age===true && params.gender===true){//code---}
//if(params.name!==false || params.age!==false || params.gender!==false){//code---}
//if(!params.name || !params.age || !params.gender){//code---}
~~~

### 4. JS动态生成的元素  绑定事件
~~~js
//事件委托
$('xx').on('事件','动态元素选择器',function(){})
~~~

### 5. data-xx 自定义的标签属性 获取值
~~~js
//<div data-id='xxx' class='text'><div/>

let value = $('.text').data('id');
console.log(value); //xxx
~~~

### 6. $.ajax  请求里面 data 数据不能是表达式
~~~js
//正确
const name=$('#xxx').val();
data:{
	name,
}

//错误-会报错-不可用
const name=$('#xxx');
data:{
	name.val(),
}

~~~

### 7. jquery 取消单选框的选中状态
~~~js
//可以
$('input[type=radio]').prop('checked',false)
$("input[name=studentGender]:checked").prop('checked',false)
$('input[type=radio]:checked').prop('checked',false)
~~~

### 8. 伪数组 转 数组---解构
~~~JS
//所游被选中的多选框

//真数组
[...$('#input[type=checkbox]:checked')]

//伪数组
$('#input[type=checkbox]:checked')
~~~

### 9. ajax 请求发送数据 有 数组形式 时, `traditional:true`,
~~~js
arr = ['a','b']
$.ajax{
	url:'',
	type:'',
	data:{
        //数组对象
		arr
	},
	//⭐⭐⭐处理数组对象格式问题
    traditional:true,
        
    success(msg){
        
    }
}
~~~

### 10. TypeError: Cannot read properties of undefined (reading 'create')
#### ！！create对应mongoose的方法
~~~js
1. 检查 const {classesModel} = require('./models/classesModel');
2. 检查 classesModel.create(params)
 总之就是检查 1.引入文件名 2.模型名 
~~~

### 11.⭐ 使用 $('').serializeArray() 注意给要收集的input加入name属性



## 遗留待优化

### 1. 新增/修改学生后 所属班级下拉选择的待复原
### 2. 学生信息排序（年龄，性别，---）
### 3. （已优化）搜索后对应的分页功能！！
### 4. 本地存储的信息可用localStorage
