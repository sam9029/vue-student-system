var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//连接数据库----------------有顺序要求
require('./dao/database.js');

//引入路由-------------------
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var studentsRouter = require('./routes/students');
var teachersRouter = require('./routes/teachers');
var classesRouter = require('./routes/classes');
var imagesRouter = require('./routes/images');
var usersRouter = require('./routes/users');

//引入token验证-----------------有顺序要求
const jwtAuth = require('./utils/jwt.js');

var app = express();

//开启跨域
var allowCrossDomain = function (req, res, next) {
    // 设置允许哪一个源（域）可以进行跨域访问，* 表示所有 源（可指定）
    res.header("Access-Control-Allow-Origin", "*");
    // 设置允许跨域访问的 请求头
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Origin,Content-Type,Accept,Authorization");
    // 设置允许跨域访问的 请求类型
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    // 设置允许 cookie 发送到服务器 
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
};
app.use(allowCrossDomain); // 使用该中间件

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//配置并使用token验证-----------------有顺序要求
app.use(jwtAuth);

//配置后端一级路由---------------------
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/students', studentsRouter);
app.use('/teachers', teachersRouter);
app.use('/classes', classesRouter);
app.use('/images', imagesRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// module.exports = app;
app.listen(3000, err =>{
  if(err) console.log('error:',err);
  else console.log('3000端口成功启动')
})
