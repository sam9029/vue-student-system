const {Schema, model} = require('mongoose');

const teachersSchema = new Schema({
    name: String,
}, { versionKey: false });

module.exports.teachersModel = model('teachersModel',teachersSchema,'teachers');

