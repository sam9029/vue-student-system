const {Schema, model} = require('mongoose');

const studentsSchema = new Schema({
    name: String,
    age: String,
    gender: String,
    classId:{
        type:Schema.Types.ObjectId,
        ref:'classesModel'
    },
    imagesName:String
}, { versionKey: false });

module.exports.studentsModel = model('studentsModel',studentsSchema,'students')

