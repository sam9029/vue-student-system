const {Schema, model} = require('mongoose');

const classesSchema = new Schema({
    name: String,
    teachers:[{
        type:Schema.Types.ObjectId,
        ref:'teachersModel'
    }]
}, { versionKey: false });

module.exports.classesModel = model('classesModel',classesSchema,'classes');

