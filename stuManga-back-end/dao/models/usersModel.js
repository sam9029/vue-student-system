const {Schema, model} = require('mongoose');

const usersSchema = new Schema({
    username: String,
    password:String,
    imagesName:String
}, { versionKey: false });

module.exports.usersModel = model('usersModel',usersSchema,'users');

