//引入model
//引入model 一定要解构来引入 
const {classesModel} = require('./models/classesModel');
const {studentsModel} = require('./models/studentsModel');


// //获取所有班级信息
// module.exports.getClasses = async function(){
// 	const result = await classesModel
// 	.find()
// 	.populate({path:'teachers'});
// 	return result;
// }

// //新增班级信息
// module.exports.addClass = async function(params){
// 	const result = await classesModel.create(params);
// 	return result;
// }


// //获取某个班级的学生个数
// module.exports.getClassStudentCount = async function(params){
// 	console.log(this)
// 	// const count = await this.getClasses();
// 	// console.log(count)
//     // const result = await studentsModel.countDocuments({classId:_id});
//     // return result;
// }

module.exports={
	//获取所有班级信息
	getClasses: async function(){
		const result = await classesModel
		.find()
		.populate({path:'teachers'});


		//遍历班级个数
		for(let i=0; i<result.length; i++){
			//！！！！取出班级id，以对象形式存入result对应的班级数组
			
			//result 中 class中_id 是 new ObjectId() 对象类型
			//将 new ObjectId() 转换成字符串
			const classId = (result[i]._id).toString();

			//利用classIdData通过studentsModel获取每个班的人数
			const classTotalStudent = await studentsModel.countDocuments({classId});

			result[i] = {
				//注意 mongoose 方法 返回的查询后对象中 ._doc 才是查询值 result[i]
				...result[i]._doc,
				classTotalStudent
			}
		}
		return result;
	},

	//新增班级信息
	addClass: async function(params){
		const result = await classesModel.create(params);
		return result;
	},

}