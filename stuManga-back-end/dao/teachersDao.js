//引入model
//引入model 一定要解构来引入 
const {teachersModel} = require('./models/teachersModel');

//获取所有教师信息
module.exports.getTeachers = async function(){
	const result = await teachersModel.find();
	return result;
}

//新增教师信息
module.exports.addTeacher = async function(params){
	const result = await teachersModel.create(params);
	return result;
}
