//引入secretkey 
const {TOKEN_SECRETKEY} = require("./secretKey")

//express-jwt 6.0版本用如下：
// 配置 token 的验证规则
const expressJWT = require('express-jwt');

const jwtAuth = expressJWT({
    secret: TOKEN_SECRETKEY,  // 生成 token 时的密钥
    algorithms: ['HS256'], // 设置 jwt 的算法
    // 对于没有携带 token 的请求也会进行验证，没有 token 则验证不通过
    // 如果设置为 false，就表示对于没有 token 的请求不进行验证，直接通过 
    credentialsRequired: true
}).unless({
    // 用于设置不需要进行身份认证的接口
    path: ['/users/login', '/users/register','/users/isExist']
})

module.exports = jwtAuth;



//express-jwt新版本用如下：
// // 配置 token 的验证规则
// const {expressjwt:jwt} = require('express-jwt');

// const jwtAuth = jwt({
//     secret: 'defaultString',  // 生成 token 时的密钥
//     algorithms: ['HS256'], // 设置 jwt 的算法
//     // 对于没有携带 token 的请求也会进行验证，没有 token 则验证不通过
//     // 如果设置为 false，就表示对于没有 token 的请求不进行验证，直接通过 
//     credentialsRequired: true
// }).unless({
//     // 用于设置不需要进行身份认证的接口
//     path: ['/users/login', '/users/register']
// })

// module.exports = jwtAuth;