//获取所有教师信息
getTeachers()

//获取所有教师信息
function getTeachers(){
	$.ajax({
		url:'/teachers/getTeachers',
		type:'get',
		// data,
		success(res){
			console.log(res);

			if(res.status){
				//使用渲染函数
				renderTeachers(res.data);
			}else{
				alert('未知错误，请检测网络，或刷新页面！');
			}
		}
	})
}

 
//渲染教师信息函数
function renderTeachers(data){
	const teacherList = $('#teacherList');
	const str = data.map(item=>{
		return `
			<label for='${item._id}'>${item.name}</label>
			<input type="checkbox" id='${item._id}' name='teachers'>&nbsp;&nbsp;			
		`
	}).join('');
	teacherList.html(str);
}

//新增教师
$('#addTeacher').click(function(){
	const teacherName = $('#teacherName').val();
	$.ajax({
		url:'/teachers/addTeacher',
		type:'post',
		data:{
			name:teacherName
		},
		success(res){
			if(res.status){
				alert(res.message);
				getTeachers();
				$('#teacherName').val('');
			}else{
				alert(res.message)
			}
		}
	})
})