//获取所有班级信息
getClasses()

//获取所有班级信息
function getClasses(){
	$.ajax({
		url:'/classes/getClasses',
		type:'get',
		// data,
		success(res){
			console.log(res);

			if(res.status){
				//使用渲染函数
				renderClasses(res.data);
			}else{
				alert('未知错误，请检测网络，或刷新页面！');
			}
		}
	})
}


//渲染班级信息函数
function renderClasses(data){
	//渲染新增学生区
	const classList = $('#classList');
	const str = data.map(item=>{
		return `
			<option value='${item._id}'>${item.name}</option>			
		`
	}).join('');
	classList.html(str);

	//渲染修改学生区
	const upgradeClassList = $('#upgradeClassList');
	upgradeClassList.html(str);	
}

//新增班级
$('#addClassBtn').click(function(){
	const className = $('#className').val();
	//多选，可能是一个伪数组，转化成真数组
	//选择，一个教师即字符串形式，两个及以上即为数组形式
	const teacherList = [...$('#teacherList>input[name="teachers"]:checked')];
	//提取数组中的教师 id 数据
	const teachers = teacherList.map(item => {
		return $(item).prop('id');
	})

	$.ajax({
		url:'/classes/addClass',
		type:'post',
		data:{
			name:className,
			teachers
		},
		//teachers 是数组数据，要用`traditional:true`声明
		traditional:true,
		success(res){
			if(res.status){
				alert(res.message);
				getClasses();
				$('#className').val('');
			}else{
				alert(res.message)
			}
		}
	})
})