# elementui-stusystem

- 对readme.md 的补充

# 问题学习总结

### element 菜单组件 设置router 后利用 index 路由跳转 

~~~html
<el-menu-item index="/stuSystem/">学生列表</el-menu-item>
<el-menu-item index="/stuSystem/addStu">新增学生</el-menu-item>
~~~

有时候自路由前面不要写 `/`

~~~js
{
    path: '/stuSystem',
    name: 'StuSystem',
    component: StuSystem,
    children: [
      {
        path: '',
        component: StuList
      },
      {
        path: 'addStu',
        component: () => import('@/components/student/AddStu.vue')
      },
    ]
  }
~~~

### Invalid prop: type check failed for prop "data". Expected Array, got Object
数据类型错误，
本次是请求返回的学生数据是数组，但是我在data里面初始申明的时候申明成了对象


### 修改动态路由 跳转时，mounted 无法获取 跳转时 动态路由的 props接受值
- 原本想 利用 mounted写方法 （拿props的接受值） 获取某个学生信息，结果不行

- 目前尝试 watch侦听 变化后 执行 获取某个学生信息  --- 成功(注意开启immediate才行)


### 使用路由mate元信息 以及 配合 keep-alive （v-if判断$route.meta.keepAlive），控制每次进入stulist组件的刷新

### ⭐⭐使用搜索的时候 要在每次搜索之前修改 当前页 为 1 ！！

### ⭐⭐在 elementui 的 表格里面渲染  图片
https://blog.csdn.net/zhouzongxin94/article/details/120859850
~~~vue
      <el-table-column label="头像" width="150">
      <!-- :src 图片的位置信息 -->
      <template slot-scope="scope">
        <el-avatar shape="square" :size="50" :src="'http://localhost:3000/img/' + (scope.row.imagesName || 'defaultImg.jpg')" fit='cover'></el-avatar>
      </template>  
    </el-table-column>
~~~


### 【ElementUI】el-table对象数组嵌套对象数组 渲染 
https://blog.csdn.net/qq_38987146/article/details/117254535?spm=1001.2101.3001.6650.10&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-10-117254535-blog-114634419.pc_relevant_antiscanv4&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-10-117254535-blog-114634419.pc_relevant_antiscanv4&utm_relevant_index=19

### element-ui table表格 一行展示数组里面的多个数据 数据渲染
https://blog.csdn.net/weixin_45068982/article/details/111607085

### el-table 组件 渲染 对象内含数组的数据
~~~js
  // 如：
  obj = {
    a:[ {}, {} ],
    b:'xx',
    c:'yyy'
  }
~~~
~~~html
    <!-- 这个 el-table-column  针对  a:[ {}, {} ] -->
    <el-table-column label="负责教师">
      <!--   -->
      <!-- 使用 <template slot-scope="scope"></template>   -->
      <template slot-scope="scope" >
        <!-- v-for='item in scope.row.classId.teachers' 写在 第一个 template 上 没有用会报错  -->
        <!-- 所以我使用两个 tempalte 来 嵌套（其实不建议tempalte嵌套写法） -->
        <template v-for='item in scope.row.classId.teachers'>

          <el-tag :key="item._id">{{item.name}}</el-tag>

        </template>
        
      </template>

    </el-table-column>
~~~






# 待优化

## 学生列表里的多选，多选删除

## 学生列表 教师列 待优化渲染

## 空值搜索，也被执行

## 图片上传超出大小限制时 不显示
- 目前超出 大小 后 依然显示

## 学生列表头像点击后放大

## 新增学生组件--头像上传的样式对齐
  - 新增完成后-清除历史
  - ~班级选项 待渲染~
  - 选择班级后 展示老师

## ⭐修改学生组件--头像预览
  - 思路：未必要在修改组件引入 上传图片组件，使用状态机来调用上传图片的接口

## 教师组件--
  - 负责班级-涉及后端
  - 操作 - 修改 删除

## 班级组件--
  - 新增班级 -- 教师选项 待渲染
  - 操作 - 修改 删除

## 用户注册 
  - 差登录

## 路由token判断
  - 使用 路由 全局 来判断
  - 使用 whiteList 白名单 来 (  `whiteList.includes(to.path)`  ) 设置登录 和 注册自由进出
  - ⭐(解决) router 的 index.js 文件 无法使用 elementui 的 `this.$notify({})`
      - http://t.zoukankan.com/wenqiangit-p-10490443.html 
  - (解决)token 提示待优化
   

## ？？？用户登出 


## ⭐(解决)（未登录时 无token）用户注册 用户名的存在 验证时 会使用 token 验证，导致死循环
  - 后端  utils jwt 设置 无需验证的 白名单 API











# 拓展

[CodeSandbox](https://codesandbox.io/dashboard)

感觉可以练手，属个人项目。两个链接都是同一个项目
https://github.com/hzzly/xyy-vue
https://hjingren.cn/2017/03/08/%E8%BF%9B%E9%98%B6vue%E5%85%A8%E5%AE%B6%E6%A1%B6/

关于axios封装 和 vuex-actions的 流程
https://www.codeleading.com/article/14162351045/

~~~js
整个的流程是在组件的created中提交dispatch，
然后通过action调用一个封装好的axios，
然后再触发mutation来提交状态改变state中的数据，
然后在组件的计算属性中获取state的数据并渲染在页面上
~~~


## ⭐⭐⭐在 vue 项目 里 的 单独 js  文件中使用elementui 组件
解决：
在该js文件中

~~~js
//1.引入vue
import Vue from 'vue';
//2.新创建一个vue实例
let v = new Vue();
//3.在新的实例上使用组件
  v.$notify.error({
    title: '错误',
    message: '未登录 或者token过期'
  });
~~~







