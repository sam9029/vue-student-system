//引入封装的axios实例
import axios from '@/http/axios'

//申明一个对象，用于包含所有的请求函数
//此处的api不需要 异步，直接返回axios
const studentApi = {

    //获取学生---++++//搜索学生
    getStudent(params) {
        return axios({
            url: "/students/getStudents",
            method: "get",
            params,
        })
    },

    //新增学生
    addStudent(data){
        return axios({
            url:'/students/addStudent',
            method:'post',
            data
        })
    },

    //删除学生
    deleteStu(params) {
        return axios({
            url: '/students/deleteStudent',
            method: 'get',
            params
        })
    },

    //通过id获取学生信息
    getStudentById(params) {
        return axios({
            url: '/students/_id',
            type: 'get',
            params
        })
    },

    //修改学生信息
    updateStu(data) {
        return axios({
            url: '/students/upgradeStuInfo',
            method: 'post',
            data
        })
    },

}

export default studentApi