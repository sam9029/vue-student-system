//引入所有的Api
import studentApi from './student.js'
import classApi from './class.js'
import teacherApi from './teacher.js'
import loginApi from './login.js'


const api = {
    studentApi,classApi,teacherApi,loginApi
}

export default api