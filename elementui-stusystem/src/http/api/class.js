//引入封装的axios实例
import axios from '@/http/axios'


//申明一个对象，用于包含所有的请求函数
//此处的api不需要 异步，直接返回axios
const classApi = {

    //获取班级---
    getClass() {
        return axios({
            url: "/classes/getClasses",
            method: "get",
        })
    },

    //新增班级
    addClass(data){
        return axios({
            url:'/classes/addClass',
            method:'post',
            data
        })
    },

}

export default classApi