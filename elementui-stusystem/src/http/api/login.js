import axios from '@/http/axios'

const loginApi = {
    //登录验证
    login(params){
        return axios({
            url:'/users/login',
            method:'get',
            params
        }) 
    },

    //注册用户名验证
    isExist(params){
        return axios({
            url:'/users/isExist',
            method:'get',
            params
        })
    },

    //注册账号
    register(data){
        return axios({
            url:'users/register',
            method:'post',
            data
        })
    },

    //是否已登录
    isLogin(){
        return axios({
            url:'users/isLogin',
            method:'get',
        })
    }
}

export default loginApi