//引入封装的axios实例
import axios from '@/http/axios'


//申明一个对象，用于包含所有的请求函数
//此处的api不需要 异步，直接返回axios
const teacherApi = {

    //获取班级---
    getTeacher() {
        return axios({
            url: "/teachers/getTeachers",
            method: "get",
        })
    },

    //新增班级
    addTeacher(data){
        return axios({
            url:'/teachers/addTeacher',
            method:'post',
            data
        })
    },

}

export default teacherApi