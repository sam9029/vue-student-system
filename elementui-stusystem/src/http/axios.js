import axios from 'axios'

//myAxios 是axios实例名称-----------------------
//axios创建实例使用  axios.create({})
const myAxios = axios.create({
    // axios 的 url 的基础路径
    baseURL: 'http://localhost:3000',
    // 用于请求超时的时间，超过timeout的时间后，会直接报错
    timeout: 9000
});

// axios 请求拦截器----------------------------
myAxios.interceptors.request.use(config => {
    // 拦截成功时执行该方法

    // 给请求头添加 token
    const token = localStorage.token;
    config.headers.Authorization = 'Bearer ' + token;
    //有请求时，展示token
    console.log('请求展示token', token);
    return config;

}, err => {
    // 拦截失败时执行该方法
    return err;
});

// axios 响应拦截器----------------------------
myAxios.interceptors.response.use(res => {
    // 响应成功执行该方法
    return res;
}, err => {
    // 响应失败执行该方法
    let message = '服务器连接失败';
    if (err && err.response) {
        switch (err.response.status) {
            case 400: message = '请求错误(400)'; break;
            case 401: message = '身份认证失败(401)'; break;
            case 403: message = '拒绝访问(403)'; break;
            case 404: message = '请求资源找不到(404)'; break;
            case 408: message = '请求超时(408)'; break;
            case 500: message = '服务器错误(500)'; break;
            case 501: message = '服务器实现(501)'; break;
            case 502: message = '网络错误(502)'; break;
            case 503: message = '服务不可用(503)'; break;
            case 504: message = '网络超时(504)'; break;
            case 505: message = 'HTTP版本不受支持(505)'; break;
            default: err.message = `链接出错(${err.response.status})!`;
        }
        return {
            data: {
                message,
                type: err.response.status,
                status: 0
            }
        }

    } else {
        return {
            data: {
                message: '服务器连接失败！',
                status: 0
            }
        }
    }

})

export default myAxios;