export default {
    namespaced: true,

    state: {
        filename: '',

        //针对修改学生的头像组件
        isShow:true
    },

    mutations: {
        changeFilename(state, newValue) {
            state.filename = newValue;
            console.log('img state',state.filename);
        },
        //针对修改学生的头像组件
        changeIsShow(state){
            state.isShow = !state.isShow;            
        }
    }
}