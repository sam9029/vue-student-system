import api from '@/http/api/apis'

export default {
    namespaced: true,

    state: {
        teacherData: []
    },

    mutations: {
        changeTeacherDate(state, newValue) {
            state.teacherData = newValue;
        }
    },

    actions: {
        async getTeacher(context) {
            console.log(api);
            const { data } = await api.teacherApi.getTeacher();
            console.log(data);

            context.commit('changeTeacherDate', data.data)
        },

        async addTeacher(context, newValue) {
            console.log(newValue);

            const { data } = await api.teacherApi.addTeacher(newValue);

            return data;
        },

    }

}