import api from '@/http/api/apis'
import ElementUI from 'element-ui';
import Vue from 'vue';

let v = new Vue()

export default {
    namespaced: true,

    state: {
        studentData: {
            students: [],
            totalPage: 1,
            totalStudent: 2,
            currentPage: 1,
            pageSize: 5,
            searchType: 'name',
            searchValue: '',
        },
    },

    mutations: {
        //测试用--获取state的数据
        getState(state) {
            console.log(state);
        },

        //获取学生后的数据更新
        getStudentsAfter(state, newData) {
            state.studentData = {
                ...state.studentData,
                ...newData
            };
            // console.log(state.studentData);
        },

        //分页功能的数据修改
        toggleCurrentPage(state, newValue) {
            state.studentData.currentPage = newValue
        },
        //页数据条数修改
        togglePageSize(state, newValue) {
            state.studentData.pageSize = newValue
        },

        //搜索类型修改
        changeSearchType(state, newValue) {
            state.studentData.searchType = newValue;
        },
        //搜索值修改
        changeSearchValue(state, newValue) {
            state.studentData.searchValue = newValue;
        },
    },

    //异步请求的actions
    // actions 里面的函数 默认携带参数 context
    actions: {
        //新增，修改，学生列表组件都要用！
        async getStudent(context) {
            const { data } = await api.studentApi.getStudent({
                ...context.state.studentData,
                students: []
            });
            if (data.status) {
                context.commit('getStudentsAfter', data.data);
                console.log(data);
            } else {
                v.$notify.error({
                    title:'错误提示',
                    message:data.message
                })
                console.log(data);
            }
        },
    },
}