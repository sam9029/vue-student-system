import api from '@/http/api/apis'

export default {
    namespaced: true,

    state: {
        classData:[]
    },

    mutations: {
        changeClassDate(state, newValue) {
            state.classData = newValue;
        }
    },

    actions:{
        async getClass(context){
            const {data} = await api.classApi.getClass();
            console.log(data);
            context.commit('changeClassDate',data.data)
        },

        async addClass(context,newValue){
            const {data} = await api.classApi.addClass(newValue);
            console.log(data);
        },

    }

}