import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/http/api/apis'

//引入模块
import student from './modules/student'
import img from './modules/img'
//规避 class 关键字
import classStore from './modules/class'
import teacher from './modules/teacher'
import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    student, img, classStore, teacher, login
  }
})
