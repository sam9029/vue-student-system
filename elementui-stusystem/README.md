# elementui-stusystem
	- 继承stu-managa-front03的readme.md

- 前言
- ⭐⭐已建构功能
- 错误和学习总结
- 待优化
- 拓展

------------------------------------

# 前言：
- api 文档在 back-end 文件夹

~~~cmd
最近 npm i 出错 
更新 npm i -g npm 到最新版本
使用 CMD npm i

npm config set registry https://registry.npm.taobao.org
//原有---有时候原有的才能无错误下载
npm config set registry https://www.npmjs.org
阿里云和清华 网易镜像用不起


npm i -g @vue/cli
~~~

~~~shell
set-ExecutionPolicy RemoteSigned
~~~

## vue : 无法加载文件 

> https://blog.csdn.net/wqnmlgbsz/article/details/100654258

- 1.powershell 设置 set-ExecutionPolicy RemoteSigned 

- 2. 或者 配置 环境变量

## VS code 保存时格式化文档

> https://jingyan.baidu.com/article/a3f121e4e73e26bd9152bb6a.html

------------------------------------


# ⭐⭐已建构功能
## 页面
	- 登录
	- 注册
	- 主页
		- 学生
			- 列表
			- 新增
			- 修改
		- 班级
			- 列表
			- 新增
		- 教师
			- 列表
			- 新增

## 具体功能
### 登录验证
### 注册验证
### 用户名存在验证
### 是否登录验证
### 获取所有学生信息
### 分页
### 新增学生
### 搜索学生
### 修改学生
### 新增教师
### 新增班级

## 重要功能逻辑
### 1. 代理服务器端口 -- 不需要了
	- 使用axios 封装基础url 后 在后端 使用cors 开启了跨域请求，不再使用vue代理

	- 代理步骤在根目录 vue.config.js 文件
	```js
	module.exports = {
		devServer={
			//这里后端的接口是3000
			proxy:'http://localhost:3000'
		}
	}
	```
	

### 2. 封装 axios 请求
~~~js
import axios from 'axios'

//myAxios 是axios实例名称-----------------------
//axios创建实例使用  axios.create({})
const myAxios = axios.create({
    // axios 的 url 的基础路径
    baseURL: 'http://localhost:3000',
    // 用于请求超时的时间，超过timeout的时间后，会直接报错
    timeout: 9000
});

// axios 请求拦截器----------------------------
// axios 请求拦截器----------------------------
myAxios.interceptors.request.use(config => {
    // 拦截成功时执行该方法

    // 给请求头添加 token
    const token = localStorage.token;
    config.headers.Authorization = 'Bearer ' + token;
    //有请求时，展示token
    console.log('请求展示token', token);
    return config;

}, err => {
    // 拦截失败时执行该方法
    return err;
});

// axios 响应拦截器----------------------------
// axios 响应拦截器----------------------------
myAxios.interceptors.response.use(res => {
    // 响应成功执行该方法
    return res;
}, err => {
    // 响应失败执行该方法
    let message = '服务器连接失败';
    if (err && err.response) {
        switch (err.response.status) {
            case 400: message = '请求错误(400)'; break;
            case 401: message = '身份认证失败(401)'; break;
            case 403: message = '拒绝访问(403)'; break;
            case 404: message = '请求资源找不到(404)'; break;
            case 408: message = '请求超时(408)'; break;
            case 500: message = '服务器错误(500)'; break;
            case 501: message = '服务器实现(501)'; break;
            case 502: message = '网络错误(502)'; break;
            case 503: message = '服务不可用(503)'; break;
            case 504: message = '网络超时(504)'; break;
            case 505: message = 'HTTP版本不受支持(505)'; break;
            default: err.message = `链接出错(${err.response.status})!`;
        }
        return {
            data: {
                message,
                type: err.response.status,
                status: 0
            }
        }

    } else {
        return {
            data: {
                message: '服务器连接失败！',
                status: 0
            }
        }
    }

})

export default myAxios;
~~~

### 3. 创建vue-cli时预选vue-router 和 vueX
	- router 动态路由，嵌套路由使用
	- vux模块化使用

### 4. 动态路由 url `/xxx/:info` 携带信息
	- 从stuList 到 UpdateStu,使用路由携带学生id信息
	- 转到UpdateStu后，axios请求，获取要修改的学生信息，并渲染数据
	- 通过 <button @click="$router.push('/updateStu/'+item._id)" tag='button'>修改</button>

### 5. 导航守卫 
	- 全局守卫 -- 可用于验证token（另外一种是直接在axios封装js文件啊中验证）
	- 路由独享守卫
	- 组件守卫

### 全接口验证token方式
	- 在router 文件中全局守卫
	- （另外一种是直接在axios封装js文件啊中验证，可在js文件引入router）

### ⭐6.利用vuex 封装axios 的使用
### ⭐6.利用vuex 封装axios 的使用
	- 注意axios封装后使用流程
	- 封装axios文件，
	- 具体api文件 进行 import 调用
	- 组件 import api文件进行调用
	- 流程示意： axios.js---xxAPi.js---api.js---vuex的actions调用---组件调用actions




----




# 错误和学习总结

### 0. ⭐组件内的data 要使用 `data(){ return { } }`

### 1. v-for 使用 （遍历数组、对象的不同）

```js
//遍历数组
v-for='item in arr'

obj={name:'zzz',age:'23'}
//遍历对象
v-for='(val,key,i) in obj'
//val 是值
//key 是键
//i是索引
```

### 2. axios使用（实例时基本用的异步）

- 使用时记得引入`import axios from 'axios'`

- 基本语法

```js
axios({
	url:'',
	method:'',
	param/data:{},
})
```

- 实例使用

```js
//方法一（ES7 用法）
async function(){
	const data = await axios({
        url:'',
        method:'',
        param/data:{},
	})
	if(data.status){
		//axios成功后处理
		console.log(data)
		//code
	}
}

//方法二（ES6 用法）
async function(){
	const msg = await axios({
        url:'',
        method:'',
        param/data:{},
	}).then( (msg) => {
            //axios成功后处理
            console.log(msg)
            //code
        }  	
    }
)	
}
```

### 3. 渲染图片

- src 是动态的 所以要v-bind

```vue
<tr v-for='item in studentData.students' :key='item._id'>
    <!-- 渲染图片-->
    <td>
         <!-- http://localhost:3000 定位到后端 public 文件 -->
    	<img :src="'http://localhost:3000/img/' + ( item.imageName || 'defaultImg.jpg' )" />
    </td>
    <td>{{item.name}}</td>
    <td>{{item.age}}</td>
    <td>{{item.gender}}</td>
    <td>{{item.classId.name}}</td>
    <td>
        <button>修改</button>
        <button>删除</button>
    </td>
</tr>
```

### 4. 数据侦听（修改时，从父组件接受值存入data）
#### 拓展 ：Vue中使用watch监听Vuex中的数据变化：https://blog.csdn.net/u011332271/article/details/124795930

#### vue侦听 deep 和  immediate 
https://blog.csdn.net/weixin_43907332/article/details/103037779?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1-103037779-blog-122869546.pc_relevant_antiscanv4&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1-103037779-blog-122869546.pc_relevant_antiscanv4&utm_relevant_index=1

#### vue2.0——监听props、vuex变化
https://blog.csdn.net/HarryHY/article/details/109829846?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_utm_term~default-4-109829846-blog-100704884.pc_relevant_default&spm=1001.2101.3001.4242.3&utm_relevant_index=7

```vue
export default{
	props:{

	}
	data(){
		return
	}
}

```

### 5. 组件里面 props 和 data 的区别

props：接受值，来源值变化时，可变

data：要return写法；来源值变化时，不会自动变；

### ⭐6. 搜索时，记得将currentPage,改为1先

### ⭐7. v-bind 使用规则？？？？

### 8. 调用函数  this.getStudents();
	- 不是这样 this.getStudents;
	- 记得加上()!!!!

### 9. 搜索之前，记得修改 页码为 一！！

### Git 之 获取远程分支（fetch5种方法），并合并到本地分支
(Git 之 获取远程分支（fetch5种方法），并合并到本地分支（merge和rebase的2种方法）中)[https://blog.csdn.net/u014361280/article/details/109047336?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1-109047336-blog-117906902.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1-109047336-blog-117906902.pc_relevant_default&utm_relevant_index=2]




- 注意：unescape() 函数已经从 Web 标准中删除，所以尽量不使用该函数，可以使用 (decodeURI)[https://www.runoob.com/jsref/jsref-decodeuri.html] 或 (decodeURIComponent)[https://www.runoob.com/jsref/jsref-decodeuricomponent.html] 代替。(这里不知道为什么 都使用不了，，)

目前使用btoa（加密） atob（解密）
~~~js
  const escape1 = btoa('xxxxx');
  console.log(escape1);
  const unescape1 = atob(escape1);
  console.log(unescape1);
~~~


---
## 待优化（有些已解决）

### 分页功能的获取刷新，没有防抖
	- mutations 不能 调用actions的方法，反之则可
	- 所有的分页共功能都在一个mutations方法togglePageDate内，没有防抖

## 学生列表里的多选，多选删除

## 学生列表 教师列 待优化渲染

## 空值搜索，也被执行

## 图片上传超出大小限制时 不显示
- 目前超出 大小 后 依然显示

## 学生列表头像点击后放大

## 新增学生组件--头像上传的样式对齐
  - 新增完成后-清除历史
  - ~班级选项 待渲染~
  - 选择班级后 展示老师

## ⭐修改学生组件--头像预览
  - 思路：未必要在修改组件引入 上传图片组件，使用状态机来调用上传图片的接口

## 教师组件--
  - 负责班级-涉及后端
  - 操作 - 修改 删除

## 班级组件--
  - 新增班级 -- 教师选项 待渲染
  - 操作 - 修改 删除

## 用户注册 
  - 差登录

## 路由token判断
  - 使用 路由 全局 来判断
  - 使用 whiteList 白名单 来 (  `whiteList.includes(to.path)`  ) 设置登录 和 注册自由进出
  - ⭐(解决) router 的 index.js 文件 无法使用 elementui 的 `this.$notify({})`
      - http://t.zoukankan.com/wenqiangit-p-10490443.html 
  - (解决)token 提示待优化
   

## 用户登出 


## ⭐(解决)（未登录时 无token）用户注册 用户名的存在 验证时 会使用 token 验证，导致死循环
  - 后端  utils jwt 设置 无需验证的 白名单 API




# 拓展

## (使用Gitee Pages服务 搭建Vue项目)[https://blog.csdn.net/LitongZero/article/details/88207833]

[CodeSandbox](https://codesandbox.io/dashboard)

感觉可以练手，属个人项目。两个链接都是同一个项目
https://github.com/hzzly/xyy-vue
https://hjingren.cn/2017/03/08/%E8%BF%9B%E9%98%B6vue%E5%85%A8%E5%AE%B6%E6%A1%B6/

关于axios封装 和 vuex-actions的 流程
https://www.codeleading.com/article/14162351045/

~~~js
整个的流程是在组件的created中提交dispatch，
然后通过action调用一个封装好的axios，
然后再触发mutation来提交状态改变state中的数据，
然后在组件的计算属性中获取state的数据并渲染在页面上
~~~


## ⭐⭐⭐在 vue 项目 里 的 单独 js  文件中使用elementui 组件
解决：
在该js文件中

~~~js
//1.引入vue
import Vue from 'vue';
//2.新创建一个vue实例
let v = new Vue();
//3.在新的实例上使用组件
  v.$notify.error({
    title: '错误',
    message: '未登录 或者token过期'
  });
~~~