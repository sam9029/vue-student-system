# 项目说明

- elementui-stusystem 文件是前端
    - （本次重点）vue框架写
    - 使用了elementui 组件
    - 功能详情readme

- back-end 是充当后端
    - 本身就是一个全栈项目
    - 技术详情见文件中的readme

- 弃用为以往的版本

# 分支说明
- (主要使用)devTest 用于上线测试
- master 为成功版本存档
- dev 成功版本存档的测试副本
 
 其他分支不用管（都是测试用）


# api扩展：
api free https://www.free-api.com/

api接口仓库 https://github.com/lyx32609/free-api-interface

如何创建vue项目并使用element框架
https://baijiahao.baidu.com/s?id=1676867200891645768&wfr=spider&for=pc
https://blog.csdn.net/weixin_40571331/article/details/106752954
