//图片操作

//新增学生--图片上传 stuImage
//含上传（复制）到后端文件，上传成功后的预览渲染
$('#uploadImg').change(function(e){
	//获取图片相关信息
	const files = this.files;
	//JS 原生的表单对象
	const formData = new FormData();
	//获取图片img标签，准备渲染
	const stuImage = $('#stuImageUpload');
	
	//将图片存进JS 原生的表单对象
	formData.append('file',files[0])

	//上传图片的ajax处理
	$.ajax({
		url:'/images/uploadImg',
		//图片上传一定要用post
		type:'post',
		//使用JS 原生表单对象 formData 传输图片数据
		data:formData,

		//以下三个默认加上
		cache:false, //不读取缓存结果
		contentType:false, //数据编码不适用jquery格式
		processData:false, //发送图片不转换其他格式

		success(res){
			console.log(res);
			if(res.status){
				console.log(res.message);
				stuImage
					.prop('src',`./temp/${res.filename}`)
					//自定义data-filename属性保存图片名称，供新增学生调用
					.data('filename',res.filename);
			}else{
				alert(res.message);
			}
		}
	})
})

//修改学生--图片上传
$('#upgradeImg').change(function(e){
	//获取图片相关信息
	const files = this.files;
	//JS 原生的表单对象
	const formData = new FormData();
	//获取图片img标签，准备渲染
	const stuImage = $('#stuImageUpgrade');
	
	//将图片存进JS 原生的表单对象
	formData.append('file',files[0])

	//上传图片的ajax处理
	$.ajax({
		url:'/images/uploadImg',
		//图片上传一定要用post
		type:'post',
		//使用JS 原生表单对象 formData 传输图片数据
		data:formData,

		//以下三个默认加上
		cache:false, //不读取缓存结果
		contentType:false, //数据编码不适用jquery格式
		processData:false, //发送图片不转换其他格式

		success(res){
			console.log(res);
			if(res.status){
				console.log(res.message);
				stuImage
					.prop('src',`./temp/${res.filename}`)
					//自定义data-filename属性保存图片名称，供新增学生调用
					.data('filename',res.filename);
			}else{
				alert(res.message);
			}
		}
	})
})