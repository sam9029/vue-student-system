//同意处理ajax请求头，携带token验证处理
$.ajaxSetup({
    // 将 token 添加到请求头中
    headers: {
        Authorization: 'bearer '+ localStorage.token
    },
    error(err){
        if(err.status == 401){
            alert('你还未登录，请先登录');
            location.href = './login.html'
        }
    }
})


//处理token
isLogin();

//登录验证-token
function isLogin(){
	console.log(localStorage.token);
	$.ajax({
		url:'users/isLogin',
		type:'get',
		headers:{
			//要加  'Bearer ' 在前面
        	//Bearer后面要用空格，
			Authorization: 'Bearer ' + localStorage.getItem('token')

		},
		success(res){
			console.log(res);
			// //获取所有学生信息
			// getStudents(); 
			// //获取所有教师信息
			// getTeachers()
			// //获取所有班级信息
			// getClasses()
		},
		error(err){
			if(err) console.log('error-',err);
		}
	})
}