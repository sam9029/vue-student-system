//登录表单和注册表单切换的基础操作
//去登录
$('form#register').on('click','#goLogin',e=>{
	e.preventDefault();
	$('form#login').css('display','block');
	$('form#register').css('display','none');
})
//去注册
$('form#login').on('click','#goRegister',e=>{
	e.preventDefault();
	$('form#register').css('display','block');
	$('form#login').css('display','none');
})
//忘记密码
$('form#login').on('click','#forgetPW',e=>{
	e.preventDefault();
	console.log(e);
})

//登录请求
$('#loginBtn').click(e=>{
	e.preventDefault();
	//得到一个数组
	const formInfo = $("form#login").serializeArray();
	//提取不为空的值
	const arr = formInfo.filter(item => item.value);
	//转换为对象
	let data = {}; 
	arr.forEach(item => {
		data[item.name]=item.value;
	});

	$.ajax({
		url:'/users/login',
		type:'get',
		data:data,
		success(res){
			if(res.status){
				alert(res.message);
				//浏览器本地储存token
				localStorage.setItem('token',res.token);
				//清空输入历史
				clearInput();
				location.replace('./index.html');
			}else{
				alert(res.message);
			}
		}
	})
})

//用户名存在验证-注册
$('#reusername').change(e=>{
	const username = e.target.value;
	//注册按钮回到原始状态
	$("#registerBtn")
		.prop('disabled',false)
		.css('cursor','default');

	$.ajax({
		url:'/users/isExist',
		type:'get',
		data:{
			username
		},
		success(res){
			if(res.status){
				console.log(res.message);
			}else{
				if(res.message==='用户名已存在，不可注册'){
					//禁用注册按钮
					$("#registerBtn")
						.prop('disabled',true)
						.css('cursor','not-allowed');
					
				}
				alert(res.message)
			}
		}
	})
})

//注册请求
$('#registerBtn').click(e=>{
	e.preventDefault();
	//得到一个数组
	const formInfo = $("form#register").serializeArray();
	//提取不为空的值
	const arr = formInfo.filter(item => item.value);
	//转换为对象
	let data = {}; 
	arr.forEach(item => {
		data[item.name]=item.value;
	});

	$.ajax({
		url:'users/register',
		type:'post',
		data:data,
		success(res){
			if(res.status){
				alert(res.message)
				//清空历史
				clearInput();
			}else{
				alert(res.message)
			}
		}
	})
})

//清空登录注册输入历史函数--成功后才执行
function clearInput(){
	const inputTexts = [...$('input[type=text]')];
	inputTexts.forEach(item => {
		$(item).val('');
	})
}

//退出登录请求
//删token，在重新isLogin，退回login界面