//路由文件要配置express，和 express.Router()
//暴露路由
var express = require('express');
var router = express.Router();
module.exports = router;

// console.log('111','请求成功'); 
// -----------------------------------------------------------
 
//引入服务层--------------------------------------------------
//引入服务层--------------------------------------------------
const {
    getStudents,
    // searchStudents, 目前已弃用
    addStudent,
    upgradeGetStudent,
    upgradeStuInfo,
    deleteStudent} = require('../service/studentsService.js');
    

//引入图片处理方法
const {moveFiles,deleteFiles}=require('../utils/handleFiles')

//处理Ajax请求--------------------------------------------------
//处理Ajax请求------------------------------------------------

//获取所有学生信息
router.get('/getStudents', async function (req, res){    
	const data = await getStudents(req.query);
	res.send(data);
})

//搜索学生
// router.get('/searchStudents', async function (req, res){
//     const data = await searchStudents(req.query);
//     res.send(data);
// })

//新增学生信息
router.post('/addStudent', async function (req, res){
    const data = await addStudent(req.body);
    //新增成功后将图片从临时文件夹移动到永久
    if(data.status && req.body.imagesName){
        moveFiles({
            fromPath:'./public/temp',
            toPath:'./public/img',
            filename:req.body.imagesName
        })
    };
    deleteFiles('./public/temp');

    res.send(data);
})

//修改学生--通过_id获取学生信息
router.get('/_id',async function(req,res){
    const data = await upgradeGetStudent(req.query);
    res.send(data);
})

//修改学生
router.post('/upgradeStuInfo',async function(req,res){
    const data = await upgradeStuInfo(req.body);
    //新增成功后将图片从临时文件夹移动到永久
    if(data.status && req.body.imagesName){
        moveFiles({
            fromPath:'./public/temp',
            toPath:'./public/img',
            filename:req.body.imagesName
        })
    };
    deleteFiles('./public/temp');
    
    res.send(data)
})

//删除学生
router.get('/deleteStudent', async function(req,res){
    const data = await deleteStudent(req.query);
    res.send(data);
})
