//路由文件要配置express，和 express.Router()
//暴露路由
var express = require('express');
var router = express.Router();
module.exports = router;


//引入服务层--------------------------------------------------
//引入服务层--------------------------------------------------
const {
	addTeacher,
	getTeachers
} = require('../service/teachersService.js');


//获取所有教师信息
router.get('/getTeachers', async function (req, res){
    const data = await getTeachers();
    res.send(data);
})

//新增教师信息
router.post('/addTeacher', async function (req, res){
    const data = await addTeacher(req.body);
    res.send(data);
})

