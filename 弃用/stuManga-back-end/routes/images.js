var express = require('express');
var router = express.Router();
module.exports = router;

//调用 utils 工具文件夹
const { uploadFiles } = require('../utils/handleFiles');

router.post('/uploadImg', function(req, res, next) {
  const upload = uploadFiles({
    //上传成功后的图片路径
    path:'./public/temp',

    //对应append 的 file 前端请求 formData.append('file',files[0])
    key:'file', 

    //图片大小限制，单位KB
    size: 2000
  });  

  upload(req,res,(err)=>{
    if(err) {
      console.log('111---图片上传失败',err)
      res.send({
        message:'图片上传失败,图片大小限制1000kb',
        status:0
      })
    }
    else{
      console.log('111---图片上传成功',req.files);

      //req.files[0].filename 直接放在res.send会报错
      const filename = req.files[0].filename;
      res.send({
        message:'图片上传成功',
        status:1,

        //req.files是个数组
        //多个图片上传后，filename是数组
        filename
      })
    };   
  })

});

