//路由文件要配置express，和 express.Router()
//暴露路由
var express = require('express');
var router = express.Router();
module.exports = router;


//引入服务层--------------------------------------------------
//引入服务层--------------------------------------------------
const {
	addClass,
	getClasses
} = require('../service/ClassesService.js');


//获取所有班级信息
router.get('/getClasses', async function (req, res){
    const data = await getClasses();
    res.send(data);
})

//新增班级信息
router.post('/addClass', async function (req, res){
    const data = await addClass(req.body);
    res.send(data);
})

