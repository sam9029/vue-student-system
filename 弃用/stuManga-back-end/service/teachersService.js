const {
	addTeacher,
	getTeachers
} = require('../dao/teachersDao.js');

//获取所有教师信息
module.exports.getTeachers = async function(){
	const data = await getTeachers();
	return {
		message:'获取所有教师信息成功',
		status:1,
		data
	}
}

//新增教师信息
module.exports.addTeacher = async function(params){
	if(params.name){
		const data = await addTeacher(params);
		return {
			message:'添加教师信息成功',
			status:1,
			data
		};	
	}else{
		return{
			message:'请确认教师信息不为空',
			status:0		
		}
	
	}	
}
