const {
	login,
	isExist,
	register
} = require('../dao/usersDao.js');

//引入加密模块
const bcrypt = require('bcrypt');
//注册加密 密码
//登录 验证已加密的密码；

//获取头像
// module.exports.getStuImg = async function(params){
// 	if(params.username){
// 		const data = await getStuImg(params);
// 		return {
// 			message:'获取头像成功',
// 			status:1,
// 			data
// 		}
// 	}
// }


//登录
module.exports.login = async params =>{
	const {username,password} = params;
	//空值判断
	if(username && password){ 
		const result = await login(params);				
		//数据库返回后，用户信息存在与否判断
		if (result.length) {
			//用户信息验证（bcrypt加密后）
			const isUser = bcrypt.compareSync(password,result[0].password);
			//根据isUser判断用户信息是否一致
			if(isUser){
				return({
					message:'登录成功',
					status:1,
					username:result[0].username
				})
			}else{
				return({
					message:'用户名或密码错误！',
					status:0
				})
			}	
		}else{
			return({
					message:'该用户名未注册！请前往注册',
					status:0
				})
		}		
	}else{
		return({
			message:'有空值',
			status:0
		})
	}
}

//注册--用户名存在验证
module.exports.isExist = async params =>{
	if (params.username) {
		const data = await isExist(params);
		if(data.length){
			return ({
				message:'用户名已存在，不可注册',
				status:0
			})
		}
		else{
			return ({
				message:'用户名不存在，可注册',
				status:1
			})
		}
	}else{
		return ({
			message:'用户名输入值为空',
			status:0
		})
	}

}

//注册
module.exports.register = async params =>{
	const{reusername,repassword,copassword} = params;
	if(reusername && repassword && copassword){
      if(repassword === copassword){
      	  //加密密码
      	  const sercetPassword = bcrypt.hashSync(repassword,10)
          const result = await register({
            username:reusername,
            password:sercetPassword
          })
          
          return({
          	message:'注册成功',
          	status:1,
          	result
          });
      }else{
          return({
          	message:'重复密码错误',status:0
          });
      }  
  	}else{
    	return({
    		message:'有空值',status:0
    	});  
  	}
}
