const {
	addClass,
	getClasses
} = require('../dao/ClassesDao.js');

//获取所有班级信息
module.exports.getClasses = async function(){
	const data = await getClasses();
	return {
		message:'获取所有班级信息成功',
		status:1,
		data
	}
}

//新增班级信息
module.exports.addClass = async function(params){
	//params.teachers={name，teachers}
	//name 班级name，teachers （undefined 或String 或Array）
	// 老师的情况有: //空的时候 undifined 不通过 //一个老师 String 通过 //两个及以上 Array 通过
	//直接使用 params.teachers 判断
	if(params.name && params.teachers){
		const data = await addClass(params);
		return {
			message:'添加班级信息成功',
			status:1,
			// data
		};	
	}else{
		return{
			message:'请确认班级信息和老师信息不为空',
			status:0		
		}
	
	}	
}
