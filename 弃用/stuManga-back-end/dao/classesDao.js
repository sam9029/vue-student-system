//引入model
//引入model 一定要解构来引入 
const {classesModel} = require('./models/classesModel');

//获取所有班级信息
module.exports.getClasses = async function(){
	const result = await classesModel.find();
	return result;
}

//新增班级信息
module.exports.addClass = async function(params){
	const result = await classesModel.create(params);
	return result;
}
