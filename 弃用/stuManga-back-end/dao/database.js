//链接mongoDB数据库 

// 引入模块
const mongoose = require('mongoose');

// 设置要连接的 MongoDB 服务器地址(studentsManage:要连接的数据库名称)
//⭐如果没有会自动创建！！
const dbURI = 'mongodb://localhost:27017/studentSystem';

// 连接数据库
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

// ⭐当项目与数据库连接成功时，触发下列事件
mongoose.connection.on('connected', () => console.log(dbURI + ' 数据库连接成功'));