# stu-manga-front03
	- 继承stu-managa-front的readme.md

- 前言
- 错误和总结
- 待优化

------

## 前言	

### 1. 记得代理服务器端口
	- 在根目录 vue.config.js 文件
	```js
	module.exports = {
		devServer={
			//这里后端的接口是3000
			proxy:'http://localhost:3000'
		}
	}
	```

### 2. 下载 axios 包

### 3. 创建vue-cli时预选vue-router

### 4. 动态路由 url `/xxx/:info` 携带信息
	- 从stuList 到 UpdateStu,使用路由携带学生id信息
	- 转到UpdateStu后，axios请求，获取要修改的学生信息，并渲染数据
	- 通过 <button @click="$router.push('/updateStu/'+item._id)" tag='button'>修改</button>

### 5. 导航守卫 
	- 全局守卫
	- 路由独享守卫
	- 组件守卫

----

## 错误和总结

### 0. ⭐组件内的data 要使用 `data(){ return { } }`

### 1. v-for 使用

```js
//遍历数组
v-for='item in arr'

obj={name:'zzz',age:'23'}
//遍历对象
v-for='(val,key,i) in obj'
//val 是值
//key 是键
//i是索引
```

### 2. axios使用（实例时基本用的异步）

- 使用时记得引入`import axios from 'axios'`

- 基本语法

```js
axios({
	url:'',
	method:'',
	param/data:{},
})
```

- 实例使用

```js
//方法一（ES7 用法）
async function(){
	const data = await axios({
        url:'',
        method:'',
        param/data:{},
	})
	if(data.status){
		//axios成功后处理
		console.log(data)
		//code
	}
}

//方法二（ES6 用法）
async function(){
	const msg = await axios({
        url:'',
        method:'',
        param/data:{},
	}).then( (msg) => {
            //axios成功后处理
            console.log(msg)
            //code
        }  	
    }
)	
}
```

### 3. 渲染图片

- src 是动态的 所以要v-bind

```vue
<tr v-for='item in studentData.students' :key='item._id'>
    <!-- 渲染图片-->
    <td>
         <!-- http://localhost:3000 定位到后端 public 文件 -->
    	<img :src="'http://localhost:3000/img/' + ( item.imageName || 'defaultImg.jpg' )" />
    </td>
    <td>{{item.name}}</td>
    <td>{{item.age}}</td>
    <td>{{item.gender}}</td>
    <td>{{item.classId.name}}</td>
    <td>
        <button>修改</button>
        <button>删除</button>
    </td>
</tr>
```

### 4. 数据侦听（修改时，从父组件接受值存入data）

```vue
export default{
	props:{

	}
	data(){
		return
	}
}

```

### 5. 组件里面 props 和 data 的区别

props：接受值，来源值变化时，可变

data：要return写法；来源值变化时，不会自动变；

### ⭐6. 搜索时，记得将currentPage,改为1先

### ⭐7. v-bind 使用规则？？？？

### 8. 调用函数  this.getStudents();
	- 不是这样 this.getStudents;
	- 记得加上()!!!!

### 9. 搜索之前，记得修改 页码为 一！！




---
## 待优化

### 分页功能的获取刷新，没有防抖
	- mutations 不能 调用actions的方法，反之则可
	- 所有的分页共功能都在一个mutations方法togglePageDate内，没有防抖