//引入自己封装的axios 实例
import axios from '../axios.js'

const classApi = {
	addClass:function (data) {
		return axios({
			url:'/classes/addClass',
			method:'post',
			data
		})
	}
}

export default classApi;

