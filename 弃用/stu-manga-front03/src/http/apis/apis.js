import classApi from './classApi.js'
import userApi from './userApi.js'


const api = {
	classApi,userApi
}

export default api;