//引入自己封装的axios 实例
import axios from '../axios.js'

const userApi = {
	login:function(params){
		return axios({
			url:'/users/login',
			method:'get',
			params
		})
	},
	isLogin:function () {
		return axios({
			url:'users/isLogin',
			method:'get',
			//此处不用添加头部了，因为axios封装时，已经为所有的axios请求添加了头部token
		})
	}
}

export default userApi;

