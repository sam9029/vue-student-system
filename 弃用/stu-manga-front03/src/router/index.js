import Vue from 'vue'
import VueRouter from 'vue-router'
import StuList from '../views/StuList.vue'
import StuSystem from '../views/StuSystem.vue'
import api from '../http/apis/apis.js'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    component:()=> import('../views/Login.vue')
  },
  {
    path:'/register',
    component:()=> import('../views/Register.vue')
  },
  {
    path: '/stuSystem',
    //有子路由的无需设置name
    // name: 'StuSystem',
    component: () => import('../views/StuSystem.vue'),
    beforeEnter:async (to,from,next)=>{
      const {data} = await api.userApi.isLogin();
      if (data.code) {
        //防止初次登录后，'登录成功'和'用户信息获取成功'双弹出的尴尬！
        // alert(data.message);
        next();
      }else{
        alert("未登录，请先登录！");
        console.log(data)
        next('/');
      }
    },
    children:[
      {
        path:'/',
        component: () => import('../views/Home.vue'),
        meta:{keepAlive:true}
      },
      //学生--------------------------------------------------
      {
        path: '/stuList',
        name: 'StuList',
        component: StuList,
        meta:{keepAlive:false}
      },
      {
        path: '/addStu',
        name: 'AddStu',
        component: () => import('../components/students/AddStu.vue'),
        meta:{keepAlive:true}
      },
      {
        path:'/updateStu/:_id',
        props:true,
        component: () => import('../components/students/UpdateStu.vue'),
        meta:{keepAlive:true}
      },
      //教师--------------------------------------------------
      {
        path:'/teachersList',
        props:true,
        component: () => import('../components/teachers/TeachersList.vue'),
        meta:{keepAlive:true}
      },
      {
        path:'/addTeacher',
        props:true,
        component: () => import('../components/teachers/AddTeacher.vue'),
        meta:{keepAlive:true}
      },
      //班级--------------------------------------------------
      {
        path:'/classesList',
        props:true,
        component: () => import('../components/classes/ClassesList.vue'),
        meta:{keepAlive:false}
      },
      {
        path:'/addClass',
        props:true,
        component: () => import('../components/classes/AddClass.vue'),
        meta:{keepAlive:true}
      },
      //其他--------------------------------------------------
      {
        path:'/counter',
        props:true,
        component: () => import('../components/Counter.vue'),
        meta:{keepAlive:true}
      }
    ]
  },
]

const router = new VueRouter({
  routes,
  mode:"history"
})

// 全局导航守卫
// router.beforeEach((to,from,next) => {
//   const isRouter = confirm('确认跳转页面');
//   if(isRouter){
//     next()
//   }
// })

export default router
